# uniparc-domain-wstructure

[![docs](https://img.shields.io/badge/docs-v0.2-blue.svg)](https://datapkg.gitlab.io/uniparc-domain-wstructure/)
[![build status](https://gitlab.com/datapkg/uniparc-domain-wstructure/badges/master/build.svg)](https://gitlab.com/datapkg/uniparc-domain-wstructure/commits/master/)

Add structural templates to sequences in the 'uniparc-domain' repository.

## Notebooks
